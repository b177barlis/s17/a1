
let studentsList = [];


function addStudent(student){
	studentsList.push(student);
	console.log(student + " was added on a student's list.");
}


function countStudents(){
	console.log("There are total of " + studentsList.length + " student(s) enrolled.");
}


function printStudents(){
	studentsList.sort().forEach(function(student){
		console.log(student.toLowerCase());
	})

}


function findStudent(enrollee){
	let filteredStudents = studentsList.filter(function(student){
	return student.toLowerCase().includes(enrollee.toLowerCase());

	})
	
	if (filteredStudents.length == 1){
		console.log(filteredStudents + " is an enrollee.");
	}

	else if (filteredStudents.length > 1){
		console.log(filteredStudents + " are enrollees.");		
	}

	else{
		console.log("No student found with the name " + enrollee);
	}

}
	
	


